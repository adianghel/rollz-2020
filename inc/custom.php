<?php
/** embed google fonts */
function rollz_add_google_fonts() {
   wp_enqueue_style( 'wpb-google-fonts', '//fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,400;0,600;0,700;1,400;1,600;1,700&display=swap', false );
}   
add_action( 'wp_enqueue_scripts', 'rollz_add_google_fonts' );

/** add section color picker pallete */

function my_acf_input_admin_footer() {
	
?>
<script type="text/javascript">
(function($) {
	
	acf.add_filter('color_picker_args', function( args, $field ){
    // do something to args
    args.palettes = ['#f8f6f7', '#448aa6', '#fa991c', '#ffffff']
    // return
    return args;
			
  });
	
})(jQuery);	
</script>
<?php
		
}

add_action('acf/input/admin_footer', 'my_acf_input_admin_footer');

// Move Yoast to bottom
function yoasttobottom() {
	return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');

/** general filters */
function wpb_remove_version() {
  return '';
}
add_filter('the_generator', 'wpb_remove_version');
