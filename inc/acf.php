<?php
// Saving Custom fields to .json files
add_filter('acf/settings/save_json', 'my_acf_json_save_point');
function my_acf_json_save_point( $path ) {

    $path = dirname(__DIR__).'/inc/advanced-custom-fields';

    return $path;
}

// Loading Custom fields .json
add_filter('acf/settings/load_json', 'my_acf_json_load_point');
function my_acf_json_load_point( $paths ) {

    // remove original path (optional)
    unset($paths[0]);

    $paths[] = dirname(__DIR__).'/inc/advanced-custom-fields';

    return $paths;
}