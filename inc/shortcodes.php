<?php
/** shortcode black button */
function swp_btn_func( $atts ) {
    $a = shortcode_atts( array(
        'link' => '',
  'name' => 'Try it Out',
        'color' => 'green',
    ), $atts );

    return '<div class="swp-btn">
    <a style="background-color:' . $a['color'] . ';" class="" href="' . $a['link'] . '" target="_blank">' . $a['name'] . '</a>
      </div>';
}
add_shortcode( 'swp-btn', 'swp_btn_func' );