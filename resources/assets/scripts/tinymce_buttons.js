(function () {
  tinymce.PluginManager.add('swpbtn', function (editor, url) {
    // Add Button to Visual Editor Toolbar
    editor.addButton('swpbtn', {
      text: 'black button',
      cmd: 'swpbtn',
      icon: false,
    });

    editor.addCommand('swpbtn', function () {
      var selected_text = editor.selection.getContent({
        'format': 'html'
      });

      var open_column = '<div class="swp-btn"><a class="btn btn-default" href = "" rel = "nofollow" target = "_blank" > <span>${selected_text}</span> </a></div> ';
      var close_column = '';
      var return_text = '';
      return_text = open_column + close_column;
      editor.execCommand('mceReplaceContent', false, return_text);
      return;
    });

  });
})();

(function () {
  tinymce.PluginManager.add('wdm_mce_button', function (editor, url) {
    editor.addButton('wdm_mce_button', {
      text: 'Rollz Buttons',
      icon: false,
      type: 'menubutton',
      menu: [
        {
          text: 'Black Button',
          onclick: function () {
            // change the shortcode as per your requirement
            editor.insertContent('<a href="https://www.rollz.com" class="btn btn-dark">Text Button</a>');
          }
        },
        {
          text: 'Orange Button',
          onclick: function () {
            // change the shortcode as per your requirement
            editor.insertContent('<a href="https://www.rollz.com" class="btn btn-orange">Text Button</a>');
          }
        }
      ]
    });
  });
})();