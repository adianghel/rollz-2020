{{--
  Template Name: Products Page
--}}

@extends('layouts.app')
@section('content')
  <section class="section home-header">
    <img src="<?php echo get_field('header_image'); ?>" class="img-fluid"/>
    <div class="container py-5 position-absolute">
      <h1 class="white-text w-50">Takes you further</h1>
    </div>
  </section>
  @include('partials.sections.section-loop')
  @while(have_posts()) @php the_post() @endphp
    {{-- @include('partials.page-header') --}}
    {{-- @include('partials.content-page') --}}
  @endwhile
@endsection
