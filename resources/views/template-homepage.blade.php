{{--
  Template Name: Homepage Template
--}}

@extends('layouts.app')
@section('content')
  <section class="section home-header">
    <img src="<?php echo get_field('header_image'); ?>" class="img-fluid"/>
    <div class="container py-5 position-absolute">
      <h1 class="white-text w-50">Takes you further</h1>
    </div>
  </section>
  <section class="section breaker breaker--text">
    <div class="container py-5 text-center">
      <h2 class="section-title">Enjoy an active and independent life</h2>
      <p>Rollz rollators help you reach any corner of this world in comfort and style.</p>
      <div class="d-flex justify-content-center">
        <a href="#" class="btn btn-dark">Explore</a>
        <a href="#" class="btn btn-orange">Shop now</a>
      </div>
    </div>
  </section>
  @include('partials.sections.section-loop')
  <section class="section">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 newsletter p-5">
          <h3 class="section-title"><?php echo __('Stay with us!', 'rollz') ?></h3>
          <p><?php echo __('Subscribe and be the first one to know about our new products, limited collections, promotions, events and more.', 'rollz'); ?> </p>
          <form>
            <div class="form-inline">
              <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
              <button type="submit" class="btn btn-dark"><?php echo __('Sign up', 'rollz'); ?> </button>
            </div>
            <div>
              <span><?php echo __('By subscribing you agree with our <a href="#">Privacy Policty</a>.', 'rollz'); ?></span>
            </div>
          </form>
        </div>
        <div class="col-lg-6 col-insta">
          <?php echo do_shortcode( '[instagram-feed]' ); ?>
        </div>
      </div>
    </div>
  </section>
  @while(have_posts()) @php the_post() @endphp
    {{-- @include('partials.page-header') --}}
    {{-- @include('partials.content-page') --}}
  @endwhile
@endsection
