	<?php 
		$align = '';
		if ($section['2_columns']['align'] == 'text_right') {
			$align = ' order-last';
		}
  ?>
	<section class="section{{ $text_style }}" {{ $section_background_style }}>
    <div class="container">
      <div class="row">
				<div class="col-lg-6 col--text{{ $align }}">
					<?php 
						if ($section['2_columns']['text_col_image']) {
							$text_image = $section['2_columns']['text_col_image']; $size = 'full';
							echo wp_get_attachment_image( $text_image, $size, '', ["class" => "img-fluid mx-auto mb-5"] );
						}
					?>
					@if ( $section['2_columns']['title'] )
						<h2 class="section-title">{{ $section['2_columns']['title'] }}</h2>
					@endif
					@if ( $section['2_columns']['subtitle'] )
						<h3 class="section-subtitle mb-3">{{ $section['2_columns']['subtitle'] }}</h3>
					@endif
					@if ( $section['2_columns']['text'] )
						<p>@php echo $section['2_columns']['text'] @endphp</p>
					@endif
					@if ( $section['2_columns']['add_button'] )
						@php	$button = []; 
									$button['link'] = $section['2_columns']['button']['button_link'];
									$button['style'] = $section['2_columns']['button']['button_style'];
									$button['text'] = $section['2_columns']['button']['button_text'];
						@endphp
						@include('partials.elements.buttons')
					@endif
					
				</div>
				<div class="col-lg-6 col--image">
					<?php 
						if ($section['2_columns']['image']) {
							$image = $section['2_columns']['image']; $size = 'full';
							echo wp_get_attachment_image( $image, $size, '', ["class" => "img-fluid mx-auto"] );
						}
					?>  
				</div>
      </div>     
    </div>
  </section>