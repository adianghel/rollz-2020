<section class="section" {{ $section_background_style }}>
	<div class="container">
		<div class="row">
			<div class="col-12">
				@if ( $section['2_posts']['section_title'] )
					<h2 class="section-title w-100 text-center">{{ $section['2_posts']['section_title'] }}</h2>
				@endif
			</div>
			@if ($section['2_posts']['posts'])
				@foreach ($section['2_posts']['posts'] as $post)
					<div class="col-lg-6 d-flex flex-column {{ $loop->index == 1 ? ' pl-lg-5' : ' pr-lg-5' }}">
						<?php $image = $post['image']; $size = 'full'; ?>
						<?php echo wp_get_attachment_image( $image, $size, '', ["class" => "img-fluid mx-auto mb-5"] ); ?>
							@if ( $post['title'] )
								<h3 class="section-subtitle mb-3 w-75">{{ $post['title'] }}</h3>
							@endif
							@if ( $post['text'] )
								@php
									echo apply_filters('the_content', $post['text']);
								@endphp
							@endif
							@if ( $post['link'] )
								<div class="mt-auto">
									<a href="{{ $post['link'] }}" class="btn btn-dark ml-0">{{ $post['link_text'] }}</a>
								</div>
							@endif
					</div>
				@endforeach
		</div> 
		@endif    
	</div>
</section>