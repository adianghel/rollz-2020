  <section class="section section--tabs" {{ $section_background_style }}>
    <div class="container text-center {{ $vertical_padding }}">
      @if ( $section['tabs']['section_title'] )
        <h2 class="section-title w-100">{{ $section['tabs']['section_title'] }}</h2>
      @endif
      @if ($section['tabs']['tabs_list'])
        <nav>
          <div class="nav nav-tabs" id="nav-tab" role="tablist">
            @foreach ($section['tabs']['tabs_list'] as $tab)
              <a class="nav-item nav-link{{ $loop->index == 0 ? ' active' : ''  }} pl-0" id="nav-{{ $loop->index }}-tab" data-toggle="tab" href="#nav-{{ $loop->index }}" role="tab" aria-controls="nav-{{ $loop->index }}" aria-selected="true">{{ $tab['tab_label'] }}</a>
            @endforeach
          </div>
        </nav>

        <div class="tab-content" id="nav-tabContent">
          @foreach ($section['tabs']['tabs_list'] as $tab)
            <div class="tab-pane fade show{{ $loop->index == 0 ? ' active' : ''  }} py-5 row" id="nav-{{ $loop->index }}" role="tabpanel" aria-labelledby="nav-{{ $loop->index }}-tab">
              <div class="col-lg-6 text-left pr-lg-5">
                <h3 class="section-subtitle">@php echo $tab['title'] @endphp</h3>
                @php echo $tab['text'] @endphp
                @if ($tab['link'])
                  <a href="{{ $tab['link'] }}" class="btn btn-dark ml-0">@php echo __('Explore', 'rollz'); @endphp</a>
                @endif
              </div>
              <?php $image = $tab['image']; $size = 'full'; ?>
              <div class="col-lg-6 pl-lg-5 image-wrap">
                <?php echo wp_get_attachment_image( $image, $size, '', ["class" => "img-fluid mx-auto prod-image"] ); ?>
              </div>
            </div>
          @endforeach
        </div>
      @endif
      
    </div>
  </section>