  <section class="section section--features" {{ $section_background_style }}>
    <div class="container text-center">
      @if ( $section['key_features']['section_title'] )
        <h2 class="section-title w-100">{{ $section['key_features']['section_title'] }}</h2>
      @endif
      @if ($section['key_features']['columns'])
        <div class="row d-flex">
          @foreach ($section['key_features']['columns'] as $column)
            <div class="col-lg-4 px-lg-5 item d-flex flex-column my-3">
              @if ( $column['title'] )
							  <h3 class="section-subtitle text-center mb-4">{{ $column['title'] }}</h3>
							@endif
              <?php $image = $column['image']; $size = 'full'; ?>
              <div class="image-wrap">
                <?php echo wp_get_attachment_image( $image, $size, '', ["class" => "img-fluid mx-auto prod-image"] ); ?>
              </div>
              <p class="pt-3 text-center">@php echo $column['text'] @endphp</p>
            </div>
          @endforeach
        </div>
      @endif   
    </div>
  </section>