@foreach(get_field('sections') as $section)
    @php
      $section_background_type = $section['background_type'];
      $section_background = $section['background_color'];
      $section_background_style = '';
      $text_style = '';
      if($section['section_vertical_padding'] == 'zero') {
        $vertical_padding = 'py-0';
      }
      

      if ( $section_background == '#448aa6' || $section_background == '#fa991c' || $section['background_type'] == 'image' ) {
        $text_style = ' section--light-text';
      }
      if($section['background_type'] == 'image') {
        $section_background_style = 'style=background-image:url('.$section['background_image'].');';
      } else {
        $section_background_style = 'style=background-color:'.$section_background.';';
      }
    @endphp
    @if ($section['type'] == '3products')
      @include('partials.sections.3-products')
    @elseif ($section['type'] == '2columns')
      @include('partials.sections.2-columns')
    @elseif ($section['type'] == '2posts')
      @include('partials.sections.2-posts')
    @elseif ($section['type'] == 'breaker')
      @include('partials.sections.breaker')
    @elseif ($section['type'] == 'tabs')
      @include('partials.sections.tabs')
    @elseif ($section['type'] == 'key_features')
      @include('partials.sections.key-features')
    @endif
@endforeach