	<section class="section breaker--text" {{ $section_background_style }}>
    <div class="container text-center {{ $vertical_padding }}">
      <div class="row">
				<div class="col-12">
					@if ( $section['breaker']['section_title'] )
						<h2 class="section-title w-100">{{ $section['2_posts']['section_title'] }}</h2>
					@endif
					@if ( $section['breaker']['text'] )
						@php 
							echo apply_filters('the_content', $section['breaker']['text']);
						@endphp
					@endif
				</div>
    </div>
  </section>