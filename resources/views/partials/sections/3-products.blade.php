  <section class="section section--products section--grey">
    <div class="container text-center">
      @if ($section['3_products']['products'])
        <div class="row d-flex justify-content-cente">
          @foreach ($section['3_products']['products'] as $product)
            <div class="col-lg-4 item d-flex flex-column my-3">
              <?php $image = $product['image']; $logo = $product['logo']; $size = 'full'; ?>
              <div class="image-wrap">
                <?php echo wp_get_attachment_image( $image, $size, '', ["class" => "img-fluid mx-auto prod-image"] ); ?>
              </div>
              <p class="pt-3 text-center">@php echo $product['description'] @endphp</p>
              <div class="d-flex justify-content-center pt-3 pt-lg-5 mt-auto">
                <img src="{{ $logo }}" class="logo img-fluid" />  
                <a href="#" class="btn btn-dark">Explore</a>
              </div>
            </div>
          @endforeach
        </div>
      @endif
      
    </div>
  </section>