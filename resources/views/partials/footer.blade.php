<footer class="content-info">
  <div class="container py-5">
    {{-- @php dynamic_sidebar('sidebar-footer') @endphp --}}
    <div class="row">
      <div class="col-lg-3">
        <h4>Rollz</h4>
        <p>Rollz International</p>
        <ul>
          <li>
            Treubweg 51 bg<br />
            1112 BA  Diemen<br />
            The Netherlands
          </li>
          <li class="pt-2">
            +31 20 362 2010<br />
            info@rollz.com
          </li>
        </ul>
      </div>
      <div class="col-lg-3">
        <h4>Rollators</h4>
        <ul>
          <li>Rollz Motion</li>
          <li>Rollz Flex</li>
          <li>Rollz Accessories</li>
          <li>User stories</li>
          <li>Where to buy a Rollz </li>
        </ul>
      </div>
      <div class="col-lg-4">
        <h4>Service</h4>
        <ul>
          <li>Download the Rollz brochure</li>
          <li>Frequently asked questions</li>
          <li>Look into the Rollz manuals</li>
          <li>News from Rollz</li>
          <li>About Rollz International</li>
          <li>Subscribe to the Rollz newsletter</li>
          <li>Register your Rollz rollator</li>
          <li>Contact Rollz</li>
        </ul>
      </div>
      <div class="col-lg-2 social-icons">
        <h4>Rollz Social</h4>
        <div class="d-flex">
          <a href="#"><img src="@asset('images/icon-social-instagram.png')" width="58" alt="{{ get_bloginfo('name', 'display') }}" class="social" /></a>
          <a href="#"><img src="@asset('images/icon-social-facebook.png')" width="58" alt="{{ get_bloginfo('name', 'display') }}" class="social" /></a>
        </div>
        <div class="d-flex">
          <a href="#"><img src="@asset('images/icon-social-twitter.png')" width="58" alt="{{ get_bloginfo('name', 'display') }}" class="social" /></a>
          <a href="#"><img src="@asset('images/icon-social-youtube.png')" width="58" alt="{{ get_bloginfo('name', 'display') }}" class="social" /></a>
        </div>
      </div>
    </div>
  </div>
  <div class="sub-footer py-3">
    <div class="container text-center d-lg-flex justify-content-lg-center">
      <a href="#">Terms and conditions</a>
      <a href="#">Proclaimer</a>
      <a href="#">Privacy statement</a>
      <span>Chamber of Commerce registration: 34389218 </span>
      <span>© Rollz International 2020</span>
    </div>
  </div>
</footer>
