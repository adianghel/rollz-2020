<header class="banner">
  <div class="top-nav py-3">
    <div class="container">
    </div>
  </div>
  <div class="main-nav py-3">
    <div class="container">
      <div class="row">
        <div class="col-lg-4">
          <a class="brand" href="{{ home_url('/') }}"><img src="@asset('images/logo.svg')" width="110" alt="{{ get_bloginfo('name', 'display') }}" class="logo" /></a>
        </div>
        <nav class="nav-primary col-lg-8 d-flex align-items-center justify-content-lg-end">
          @if (has_nav_menu('primary_navigation'))
            {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
          @endif
        </nav>
      </div>
    </div>
  </div>
</header>
