module.exports = {
  'extends': 'stylelint-config-standard',
  'rules': {
    'no-empty-source': null,
    'string-quotes': "double",
    'declaration-empty-line-before': null,
    'rule-empty-line-before': null,
    'no-missing-end-of-source-newline': null,
    'at-rule-empty-line-before': null,
    'declaration-colon-space-after': null,
    'no-descending-specificity': null,
    'at-rule-no-unknown': [
      true,
      {
        'ignoreAtRules': [
          'extend',
          'at-root',
          'debug',
          'warn',
          'error',
          'if',
          'else',
          'for',
          'each',
          'while',
          'mixin',
          'include',
          'content',
          'return',
          'function',
          'tailwind',
          'apply',
          'responsive',
          'variants',
          'screen',
        ],
      },
    ],
  },
};
